package com.example;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;

public class RateParser {

    public static void main(String[] args) {
        String date = "22.09.2020";
        com.example.RateParser currentRate = new com.example.RateParser();
        System.out.println(currentRate.getRate(date));
    }

    private String saveXMLFromSite(String date) {
        try {
            URL url = new URL("http://www.cbr.ru/scripts/XML_daily.asp?date_req=" + date);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line = reader.readLine();
            reader.close();
            if (line.length() > 0) {
                String fileName = "rate.xml";
                PrintWriter out = new PrintWriter(fileName);
                out.println(line);
                out.close();
                return fileName;
            } else throw new IllegalArgumentException("Wrong data");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
    private static String getRateFromXML(String fileName) throws XPathExpressionException, IOException, SAXException,
            ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document doc = docBuilder.parse(fileName);

        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPath xpath = xpathFactory.newXPath();
        Element element = (Element) xpath.evaluate("/ValCurs/Valute[@ID='R01235']/Value", doc, XPathConstants.NODE);
        String rate = element.getTextContent();
        return rate;
    }

    public String getRate(String date) {
        String fileName = saveXMLFromSite(date);
        try {
            return getRateFromXML(fileName);
        } catch (XPathExpressionException | IOException | SAXException | ParserConfigurationException e) {
            throw new IllegalStateException(e);
        }
    }
}