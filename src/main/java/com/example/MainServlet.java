package com.example;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/upload")

public class MainServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/index.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        com.example.RateParser currentRate = new com.example.RateParser();
        String date = (String) req.getAttribute("text");
        //String value = RateParser.getRateFromXML(String );
        //req.setAttribute("value", value);
        //req.setAttribute("rate", value);
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }

}