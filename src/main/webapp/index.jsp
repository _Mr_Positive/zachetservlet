<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>User Form</title>
    <script>
        function CBR_XML_Daily_Ru(rates) {
            function trend(current, previous) {
                if (current > previous) return ' ▲';
                if (current < previous) return ' ▼';
                return '';
            }

            var USDrate = rates.Valute.USD.Value.toFixed(4).replace('.', ',');
            var USD = document.getElementById('USD');
            USD.innerHTML = USD.innerHTML.replace('00,0000', USDrate);
            USD.innerHTML += trend(rates.Valute.USD.Value, rates.Valute.USD.Previous);

            var EURrate = rates.Valute.EUR.Value.toFixed(4).replace('.', ',');
            var EUR = document.getElementById('EUR');
            EUR.innerHTML = EUR.innerHTML.replace('00,0000', EURrate);
            EUR.innerHTML += trend(rates.Valute.EUR.Value, rates.Valute.EUR.Previous);
        }
    </script>
    <link rel="dns-prefetch" href="https://www.cbr-xml-daily.ru/" />
    <script src="//www.cbr-xml-daily.ru/daily_jsonp.js" async></script>


</head>
<body>
<form action="" method="POST">
    Text field: <input type="text" name="text"><br>
    <input type="submit" value="submit"><br><br>
</form>

<div id="USD">Dollar now 00,0000 rub.</div>
<div id="EUR"> Euro now 00,0000 rub.</div>
<div id="USD current date"> Dollar 00,0000 rub.</div>

<p>

    <c value="${value}"></c>
</p>
</body>
</html>